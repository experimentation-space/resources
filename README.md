# Coding is cool 🎉

## Git 

- [Git](https://git-scm.com/)

### Git cheatsheet

- [ndpsoftware](https://ndpsoftware.com/git-cheatsheet.html#loc=index;)
- [Escape a git mess, step-by-step](http://justinhileman.info/article/git-pretty/git-pretty.png)

### Learning Git

- [Atlassian tutorials](https://www.atlassian.com/git/tutorials)
- [Tower](https://www.git-tower.com/learn)
- [Think Like (a) Git](https://think-like-a-git.net/)
- [Git Immersion](https://gitimmersion.com/index.html)
- [Git Time Travel Magic — Amend / Rebase](https://shift.infinite.red/git-time-travel-magic-amend-rebase-319c5a0f4e9c)
- [Git Interactive Rebase, Squash, Amend and Other Ways of Rewriting History](https://thoughtbot.com/blog/git-interactive-rebase-squash-amend-rewriting-history)
- [Atlassian git rebase](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase)
- [Git commands nobody has told you](https://bootcamp.uxdesign.cc/git-commands-nobody-has-told-you-cd7025bea8db)
- [How Does Git Actually Work?](https://www.raywenderlich.com/books/advanced-git/v1.0/chapters/1-how-does-git-actually-work)
- [How Git truly works](https://towardsdatascience.com/how-git-truly-works-cd9c375966f6)
- [Developers: Your Git will change in 2022, with Dura](https://matt-spence.com/medium/developers-your-git-will-change-with-dura)
- [Dura](https://github.com/tkellogg/dura)
- [Supercharge your Git experience](https://medium.com/@denniswalangadi/supercharge-your-git-gameplay-4ab4c7c01537)

### Git conventions

- [gitmoji 😍](https://gitmoji.dev/)
- [Conventional commit](https://www.conventionalcommits.org/en/v1.0.0/)

### Git workflows

- [Workflow Gitflow Atlassian](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow)
- [A review of Git branching strategies](https://www.learncsdesign.com/a-review-of-git-branching-strategies/)

### Git GUI

- [GitKraken](https://www.gitkraken.com/)
- [forgit](https://github.com/wfxr/forgit)

### Pull requests

- [The anatomy of a perfect pull request](https://hugooodias.medium.com/the-anatomy-of-a-perfect-pull-request-567382bb6067)
- [How one code review rule turned my team into a dream team](https://medium.com/inside-league/how-one-code-review-rule-turned-my-team-into-a-dream-team-fdb172799d11)

### Draw git graph

- [commits-graph](https://github.com/tclh123/commits-graph)
- [Git Branching Diagram](https://gist.github.com/bryanbraun/8c93e154a93a08794291df1fcdce6918)
- [gitgraph.js](https://www.nicoespeon.com/gitgraph.js/#0)

## DevOps

- [DevOps resources](https://github.com/bregman-arie/devops-resources)
- [YouTube -  TechWorld with Nana](https://www.youtube.com/c/TechWorldwithNana)

## Software engineering

### Design principles

- [The S.O.L.I.D Principles in Pictures](https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898)
- [RAII](https://fr.wikipedia.org/wiki/Resource_acquisition_is_initialization)
- [Dependency Inversion vs. Dependency Injection](https://betterprogramming.pub/straightforward-simple-dependency-inversion-vs-dependency-injection-7d8c0d0ed28e)
- [Dependency Injection Vs Dependency Inversion Vs Inversion of Control, Let’s set the Record Straight](https://medium.com/ssense-tech/dependency-injection-vs-dependency-inversion-vs-inversion-of-control-lets-set-the-record-straight-5dc818dc32d1#:~:text=The%20Inversion%20of%20Control%20is,dependencies%20to%20an%20application's%20class.)

### Design patterns

- [Refactoring Guru](https://refactoring.guru/)

### Sorftware architecture

- [Design Applications Like Trees, Not Buildings](https://betterprogramming.pub/design-applications-like-trees-not-buildings-45e5337db663)

#### Onion architecture 🧅

- [Onion Architecture 🧅](https://dev.to/barrymcauley/onion-architecture-3fgl)
- [Onion Architecture VS Three Layer](https://medium.com/swlh/onion-architecture-vs-three-layer-59a9ba2c6e02)
- [Software Architecture — The Onion Architecture](https://medium.com/@shivendraodean/software-architecture-the-onion-architecture-1b235bec1dec)
- [Domain-Driven Design & Onion Architecture](https://blog.avenuecode.com/domain-driven-design-and-onion-architecture)

#### Hexagonal architecture

- [Hexagonal architecture_](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software))
- [Hexagonal Architecture, there are always two sides to every story](https://medium.com/ssense-tech/hexagonal-architecture-there-are-always-two-sides-to-every-story-bc0780ed7d9c)
- [Hexagonal Architecture by example - a hands-on introduction](https://blog.allegro.tech/2020/05/hexagonal-architecture-by-example.html)
- [Hexagonal Architecture: three principles and an implementation example](https://blog.octo.com/hexagonal-architecture-three-principles-and-an-implementation-example/)
- [Hexagonal Architecture from the Ground Up
](https://levelup.gitconnected.com/hexagonal-architecture-from-the-ground-up-28f2a1097063)

#### Clean architecture

- [The Clean Architecture — Beginner’s Guide](https://betterprogramming.pub/the-clean-architecture-beginners-guide-e4b7058c1165)
- [The Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

#### Architecture comparisons

- [Onion vs Clean vs Hexagonal Architecture](https://medium.com/@edamtoft/onion-vs-clean-vs-hexagonal-architecture-9ad94a27da91)
- [DDD, Hexagonal, Onion, Clean, CQRS, … How I put it all together](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/)

## Python 🐍

- [Python](https://www.python.org/)
- [The Good way to structure a Python Project](https://towardsdatascience.com/the-good-way-to-structure-a-python-project-d914f27dfcc9?gi=188532bb0f6d)

### Package manager

- [pip](https://pypi.org/project/pip/)
- [pip documentation](https://pip.pypa.io/en/latest/)
- [Poetry](https://python-poetry.org/)
- [Stop using Pip, use Poetry Instead!](https://nanthony007.medium.com/stop-using-pip-use-poetry-instead-db7164f4fc72)
- [5 Reasons Why Poetry Beats Pip Python Setup](https://betterprogramming.pub/5-reasons-why-poetry-beats-pip-python-setup-6f6bd3488a04)

### GUI

- [PyQt](https://www.riverbankcomputing.com/software/pyqt/)
- [Tkinter](https://docs.python.org/fr/3/library/tkinter.html)
- [Kivy](https://kivy.org/#home)
- [Create Desktop Application Using Flask Framework](https://medium.com/@fareedkhandev/create-desktop-application-using-flask-framework-ee4386a583e9)

### CLI

- [click](https://click.palletsprojects.com/en/latest/)
- [PyInquirer](https://github.com/CITGuru/PyInquirer)
- [Designing Beautiful Command-Line Applications With Python](https://betterprogramming.pub/designing-beautiful-command-line-applications-with-python-72bd2f972ea)
- [How to print 😁😛😋🤣emojis using python🐍](https://medium.com/analytics-vidhya/how-to-print-emojis-using-python-2e4f93443f7e)
- [Introducing the Rich CLI Tool : Viewing Files in the Terminal Will Never be the Same Again](https://towardsdatascience.com/introducing-the-rich-cli-tool-viewing-files-in-the-terminal-will-never-be-the-same-again-80e7c5af5b5f)
- [A Powerful Open Source CLI for Fancy Output](https://medium.com/@april-4/a-powerful-open-source-cli-for-fancy-output-482af9063092)

### Web Frameworks

- [Django vs Flask vs FastAPI](https://www.youtube.com/watch?v=3vfum74ggHE)

#### Flask

- [Flask: web development, one drop at a time](https://flask.palletsprojects.com/en/2.1.x/)
- [The Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)

#### FastAPI

- [FastAPI](https://fastapi.tiangolo.com/)
- [The Ultimate FastAPI Tutorial](https://christophergs.com/tutorials/ultimate-fastapi-tutorial-pt-1-hello-world/)
- [Optimal way to initialize heavy services only once in FastAPI
](https://stackoverflow.com/questions/67663970/optimal-way-to-initialize-heavy-services-only-once-in-fastapi)

#### Django

- [Django](https://www.djangoproject.com/)

### Python and databases

- [Do You Know Python Has A Built-In Database?](https://towardsdatascience.com/do-you-know-python-has-a-built-in-database-d553989c87bd)
- [Integrating MongoDB with Flask](https://faun.pub/integrating-mongodb-with-flask-8f6568863c2a)
- [How to Set Up Flask with MongoDB](https://www.mongodb.com/compatibility/setting-up-flask-with-mongodb)
- [Integrating Flask With MongoDB](https://ferilukmansyah.medium.com/integrating-flask-with-mongodb-35110161d9d3)

### Data visualization

- [The Python Graph Gallery](https://python-graph-gallery.com/)

### Debugging

- [icecream](https://github.com/gruns/icecream)
- [memray - a memory profiler for Python](https://github.com/bloomberg/memray)
- [py-spy: Sampling profiler for Python programs](https://github.com/benfred/py-spy)
- [snoop](https://github.com/alexmojaki/snoop)

### Learning resources

- [The Python Tutorial](https://docs.python.org/3/tutorial/)
- [Python Tutorial W3 school](https://www.w3schools.com/python/default.asp)
- [Realpython](https://realpython.com/)
- [Google's Python Class](https://developers.google.com/edu/python)
- [Pythonspot](https://pythonspot.com/)
- [Geeksforgeeks](https://www.geeksforgeeks.org/python-programming-language/?ref=shm)
- [LEARNCSDESIGN](https://www.learncsdesign.com/)
- [TestDriven.io](https://testdriven.io/)
- [Super Fast Python!](https://superfastpython.com/)
- [Youtube - Python Simplified](https://www.youtube.com/c/PythonSimplified)
- [YouTube - ArjanCodes](https://www.youtube.com/watch?v=CvQ7e6yUtnw)
- [YouTube - Python Engineer](https://www.youtube.com/c/PythonEngineer)
- [YouTube - Corey Schafer](https://www.youtube.com/c/Coreyms/videos)
- [Youtube - mCoding](https://www.youtube.com/c/mCodingWithJamesMurphy)
- [YouTube -  Tech With Tim](https://www.youtube.com/c/TechWithTim)

### Python multi-threading

- [How to Create a Thread-Safe Singleton Class in Python](https://medium.com/analytics-vidhya/how-to-create-a-thread-safe-singleton-class-in-python-822e1170a7f6)
- [Python Condition Class | wait() Method with Example](https://www.includehelp.com/python/condition-wait-method-with-example.aspx#:~:text=wait()%20method%20is%20used,that%20case%2C%20it%20returns%20False.)
- [Coroutines and Tasks](https://docs.python.org/3.7/library/asyncio-task.html)
- [Coroutine in Python](https://www.geeksforgeeks.org/coroutine-in-python/)

### Python internal

- [What Is the Python Global Interpreter Lock (GIL)?](https://realpython.com/python-gil/)
- [Difference between __init__ , __call__ and __new__?](https://devnote.in/difference-between-__init__-__call__-and-__new__/)
- [3 Ways to create Context Managers in Python](https://medium.com/swlh/3-ways-to-create-context-managers-in-python-a88e3ba536f3)
- [Special Methods Will Change How You Write Classes in Python](https://python.plainenglish.io/special-methods-that-will-change-how-you-build-classes-in-python-cd0226b52eb6)
- [Python: What does __all__ do?](https://medium.com/@sunilrana123/python-what-does-all-do-58a9ce947454)
- [Caching in Python Using the LRU Cache Strategy](https://realpython.com/lru-cache-python/)
- [Python Decorators For Dummies](https://python.plainenglish.io/python-decorators-for-dummies-c58cdd78cf6b)
- [The single most useful Python Decorator `@cache`](https://medium.com/thedevproject/the-single-most-useful-python-decorator-cache-88086c07417e)
- [Advanced Python — What are Metaclasses?](https://medium.com/@joelbelton/advanced-python-what-are-metaclasses-c28f205aeb37)
- [Pointers in Python? Wait… Is It Possible?](https://medium.com/better-programming/pointers-in-python-wait-is-it-possible-9e19da644e41)
- [Cython for absolute beginners: 30x faster code in two simple steps](https://towardsdatascience.com/cython-for-absolute-beginners-30x-faster-code-in-two-simple-steps-bbb6c10d06ad)
- [Cleaner Python Code with Partials](https://medium.com/@bubbapora_76246/cleaner-python-code-with-partials-fef04d347390)
- [The Correct Way to Overload Functions in Python](https://martinheinz.dev/blog/50)
- [Python Closures](https://www.geeksforgeeks.org/python-closures/)

### CI/CD integration

- [Setting Gitlab CI/CD for Python application](https://medium.com/cubemail88/setting-gitlab-ci-cd-for-python-application-b59f1fb70efe)
- [Setting Up GitLab CI for a Python Application](https://www.patricksoftwareblog.com/setting-up-gitlab-ci-for-a-python-application/)
- [How To Upload Private Python Packages to Gitlab](https://medium.com/@matt_tich/how-to-upload-private-python-packages-to-gitlab-2999e9604603)
- [Poetry and GitLab: Devops for Python developers](https://docs.mpcdf.mpg.de/doc/data/gitlab/devop-tutorial.html)
- [How to Publish a Website with GitLab Pages](https://www.youtube.com/watch?v=TWqh9MtT4Bg)
- [Semantic Versioning and Release Automation on GitLab](https://levelup.gitconnected.com/semantic-versioning-and-release-automation-on-gitlab-9ba16af0c21)
- [Automatic Semantic Versioning in GitLab CI](https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/)
- [SemVer versioning: how we handled it with linear interval arithmetic](https://about.gitlab.com/blog/2021/09/28/generic-semantic-version-processing/)
- [semantic-release](https://semantic-release.gitbook.io/semantic-release/)
- [Conventional Commits](https://www.conventionalcommits.org/en/about/)
- [Commitizen](https://commitizen-tools.github.io/commitizen/)
- [Semantic Versioning In Python With Git Hooks](https://issuecloser.com/blog/semantic-versioning-in-python-with-git-hooks)
- [Python Semantic Release](https://python-semantic-release.readthedocs.io/en/latest/)
- [SemVer](https://github.com/python-semver/python-semver)
- [Semantic Release to automate publishing to PyPI ](https://guicommits.com/semantic-release-to-automate-versioning-and-publishing-to-pypi-with-github-actions/)
- [How to run a script from file in another project using include in GitLab CI?](https://stackoverflow.com/questions/63693061/how-to-run-a-script-from-file-in-another-project-using-include-in-gitlab-ci)
- [Semantic Versioning Demystifying SemVer and avoiding “dependency hell”](https://betterprogramming.pub/semantic-versioning-b24d29ca1aef)

### Python testing

- [Test Everything Easily in Python](https://python.plainenglish.io/test-everything-easily-in-python-8d98c7b0c135)
- [pytest](https://docs.pytest.org/en/7.1.x/)
- [PYTHON TESTING 101: PYTEST](https://automationpanda.com/2017/03/14/python-testing-101-pytest/#:~:text=Project%20Structure,or%20under%20the%20Python%20package.)

### Python packaging

- [Structuring Your Project](https://docs.python-guide.org/writing/structure/)
- [Introduction to Create Own Python Packages](https://medium.com/analytics-vidhya/introduction-to-create-own-python-packages-9aed417036ee)
- [Python Project Workflow](https://testdriven.io/blog/python-project-workflow/)
- [Package Python Projects the Proper Way with Poetry](https://hackersandslackers.com/python-poetry-package-manager/)
- [Python Poetry Tutorial](https://github.com/hackersandslackers/python-poetry-tutorial)

### Packages

- [httpx](https://www.python-httpx.org)
- [pydantic](https://pydantic-docs.helpmanual.io)
- [pytube](https://pytube.io)

### Bonus

- [Awesome-Python-Scripts](https://github.com/hastagAB/Awesome-Python-Scripts)
- [Awesome Python - Life is short, you need Python. ](https://awesome-python.com/)
- [Awesome Python](https://github.com/vinta/awesome-python)
- [The Hitchhiker’s Guide to Python!](https://docs.python-guide.org/)
- [Fullstack Python](https://www.fullstackpython.com/)
- [Using Prometheus to Monitor Python Applications](https://medium.com/programming-python/using-prometheus-to-monitor-python-applications-9edd0ec8329f)
- [How to configure, build, and publish your Python projects to PyPI](https://lynn-kwong.medium.com/how-to-configure-build-and-deploy-your-python-projects-to-pypi-dac40803fdf)
- [Python auto-generated documentation](https://medium.com/blueriders/python-autogenerated-documentation-3-tools-that-will-help-document-your-project-c6d7623814ef)
- [Clean Code Python](https://github.com/tvmaly/clean-code-python)
- [Build a Pre-commit Workflow to Check and Fix Your Python Code Automatically](https://betterprogramming.pub/build-a-pre-commit-workflow-to-check-and-fix-your-python-code-automatically-313deb5a5701)
- [Creating A Modern Python Development Environment](https://itnext.io/creating-a-modern-python-development-environment-3d383c944877)
- [The Good way to structure a Python Project](https://towardsdatascience.com/the-good-way-to-structure-a-python-project-d914f27dfcc9)
- [Whip up a stunning Dashboard with Python & Streamlit!](https://medium.com/@ascoolarobban/whip-up-a-stunning-dashboard-with-python-streamlit-c9dd8c224367)
- [Streamlit](https://streamlit.io/)
- [Scheduler with an API: Rocketry + FastAPI](https://itnext.io/scheduler-with-an-api-rocketry-fastapi-a0f742278d5b)
- [Building your own Network Monitor with PyShark](https://linuxhint.com/building-your-own-network-monitor-with-pyshark/)
- [How to Make a DHCP Listener using Scapy in Python](https://www.thepythoncode.com/article/dhcp-listener-using-scapy-in-python)
- [Code. Simply. Clearly. Calmly.](https://calmcode.io/)
- [Creating a Chat App in Python using UDP](https://python.plainenglish.io/chat-app-using-udp-5b486241748c)
- [A Python project template](https://medium.com/@overfittedcat/a-python-project-template-591fd9c2c310)
- [Python f-strings, Far Beyond the Expectation](https://python.plainenglish.io/python-f-strings-far-beyond-the-expectation-e2245cfef629)
- [pyscript - Run Python in Your HTML](https://pyscript.net/)
- [Pyodide](https://pyodide.org/en/stable/)
- [How a password manager works and let’s create one in python language.](https://medium.com/@kashishcharaya/how-a-password-manager-works-and-lets-create-one-in-python-language-2fd6d1bd6955)
- [Network Packet Sniffing](https://www.tutorialspoint.com/python_penetration_testing/python_penetration_testing_network_packet_sniffing.htm)
- [Store Passwords Safely in Python](https://python.plainenglish.io/store-passwords-safely-in-python-e38a8c0c8618)
- [Pendulum](https://pendulum.eustace.io/)
- [PyWebCopy](https://pypi.org/project/pywebcopy/)
- [Package Your Python Code](https://faun.pub/package-your-python-code-8a878e519210)
- [Gin provides a lightweight configuration framework for Python ](https://github.com/google/gin-config)
- [Twisted - An event-driven networking engine](https://twisted.org/)
- [How to Write Clean Codes by Using Pipe Operations in Python?](https://www.turing.com/kb/write-clean-codes-by-using-pipe-operations-in-python)
- [Best-of Python - A ranked list of awesome Python open-source libraries & tools. Updated weekly.](https://github.com/ml-tooling/best-of-python)
- [Build a Mobile Application With the Kivy Python Framework](https://realpython.com/mobile-app-kivy-python/)
- [PyArmor is a command line tool used to obfuscate python scripts](https://pyarmor.readthedocs.io/en/latest/)
- [Cerberus provides powerful yet simple and lightweight data validation functionality](https://docs.python-cerberus.org/en/stable/)
- [56 Groundbreaking Python Open-source Projects – Get started with Python](https://data-flair.training/blogs/python-open-source-projects/)
- [Scapy](https://scapy.net/)
- [Scapy](https://github.com/secdev/scapy/)
- [Dependency injection and inversion of control in Python](https://python-dependency-injector.ets-labs.org/introduction/di_in_python.html)
- [How to Build a WiFi Scanner in Python using Scapy](https://www.thepythoncode.com/article/building-wifi-scanner-in-python-scapy)
- [How To Make Parallel Async HTTP Requests in Python](https://betterprogramming.pub/how-to-make-parallel-async-http-requests-in-python-d0bd74780b8a)
- [Frida - Dynamic instrumentation toolkit for developers, reverse-engineers, and security researchers.](https://frida.re/)
- [wifi_qrcode_generator](https://github.com/lakhanmankani/wifi_qrcode_generator)
- [Rocketry: Insanely Powerful Scheduler](https://itnext.io/red-engine-insanely-powerful-scheduler-7d9d8e84b58b)
- [Documenting Python code with Sphinx](https://towardsdatascience.com/documenting-python-code-with-sphinx-554e1d6c4f6d)
- [How to create a watchdog in Python to look for filesystem changes](https://thepythoncorner.com/posts/2019-01-13-how-to-create-a-watchdog-in-python-to-look-for-filesystem-changes/)
- [Get Started with Python Watchdog](https://philipkiely.com/code/python_watchdog.html)
- [Getting Started with Python and InfluxDB](https://www.influxdata.com/blog/getting-started-python-influxdb/)
- [Get started with Grafana and InfluxDB](https://grafana.com/docs/grafana/latest/getting-started/get-started-grafana-influxdb/)
- [Creating a Backdoor in Python](https://betterprogramming.pub/creating-a-backdoor-in-python-558fd4ca2a1b)
- [Python service discovery: Advertise a service across a local network](https://stackoverflow.com/questions/21089268/python-service-discovery-advertise-a-service-across-a-local-network)

## C++ 🚀

- [RAII](https://en.cppreference.com/w/cpp/language/raii)
- [The Pimpl Pattern - what you should know ](https://www.cppstories.com/2018/01/pimpl/)
- [Pimpl using unique_ptr](https://www.fluentcpp.com/2017/09/22/make-pimpl-using-unique_ptr/)
- [Pass-by-value vs pass-by-reference-to-const](https://medium.com/@vgasparyan1995/pass-by-value-vs-pass-by-reference-to-const-c-f8944171e3ce)
- [wait and notify in C/C++ shared memory](https://stackoverflow.com/questions/2085511/wait-and-notify-in-c-c-shared-memory)
- [The Curiously Recurring Template Pattern (CRTP)](https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/)
- [Writing Common Functionality With CRTP Idiom in C++](https://betterprogramming.pub/writing-common-functionality-with-crtp-idiom-in-c-c8ccbd2dda2e)
- [Let me detach those threads for you](https://medium.com/@vgasparyan1995/let-me-detach-those-threads-for-you-2de014b26394)

### Web framework

- [Crow](https://crowcpp.org/)

## Java ☕

- [Creating a Custom Annotation in Java](https://www.baeldung.com/java-custom-annotation)
- [Java Annotations 101](https://medium.com/javarevisited/java-annotations-101-cd4d01e1470a)
- [Java: Creating and Using Custom Annotations](https://medium.com/javarevisited/java-creating-and-using-custom-annotations-a792f3d9962f)
- [A Guide to the finalize Method in Java](https://www.baeldung.com/java-finalize)
- [Guide to JNI (Java Native Interface)](https://www.baeldung.com/jni)
- [Why is there no GIL in the Java Virtual Machine? Why does Python need one so bad?](https://stackoverflow.com/questions/991904/why-is-there-no-gil-in-the-java-virtual-machine-why-does-python-need-one-so-bad)
- [Baeldung](https://www.baeldung.com/)
- [Baeldung: Mockito Tutorial](https://www.baeldung.com/mockito-series)
- [A Unit Testing Practitioner's Guide to Everyday Mockito](https://www.toptal.com/java/a-guide-to-everyday-mockito#:~:text=To%20create%20a%20spy%2C%20you,further%20description%20of%20verify()%20).)
- [javadoc.io](https://javadoc.io/)
- [Mockito Spy](https://javadoc.io/doc/org.mockito/mockito-core/2.27.0/org/mockito/Spy.html)
- [Concurrency and parallelism in Java](https://medium.com/@peterlee2068/concurrency-and-parallelism-in-java-f625bc9b0ca4)
- [100+ Core Java Interview Questions and Answers for Freshers and Experienced in 2022](https://www.simplilearn.com/tutorials/java-tutorial/java-interview-questions)
- [Appium java-client](https://github.com/appium/java-client)
- [Coroutines in pure Java](https://medium.com/@esocogmbh/coroutines-in-pure-java-65661a379c85)
- [Is it possible to test two application simultaneously through Selendroid?](https://github.com/selendroid/selendroid/issues/551)
- [What is a daemon thread in Java?](https://stackoverflow.com/questions/2213340/what-is-a-daemon-thread-in-java)
- [80 Common Java Interview Questions](https://cbarkinozer.medium.com/75-common-java-interview-questions-fd094785f7c0)

### OSGi

- [OSGi Wikipedia](https://en.wikipedia.org/wiki/OSGi)
- [The OSGi Framework](https://www.ibm.com/docs/en/was-nd/8.5.5?topic=applications-osgi-framework)
- [OSGi Subsystems and Why You Want Them](https://liferay.dev/blogs/-/blogs/osgi-subsystems-and-why-you-want-them)
- [First OSGi Bundle](https://github.com/desi109/osgi-and-java)
- [Chapter 4. Packaging your enterprise OSGi applications](https://livebook.manning.com/book/enterprise-osgi-in-action/chapter-4/1)

## Go

- [The Go programming language](https://go.dev/)
- [Hexagonal Architecture in Go](https://medium.com/@matiasvarela/hexagonal-architecture-in-go-cfd4e436faa3)
- [Awesome Go](https://awesome-go.com/)
- [Cobra - A Framework for Modern CLI Apps in Go](https://cobra.dev/)
- [Viper - Go configuration with fangs!](https://github.com/spf13/viper)
- [Building A Terminal User Interface With Golang](https://earthly.dev/blog/tui-app-with-go/)
- [The internal of go-prompt: How to control the rich terminal UI (Part I).](https://c-bata.medium.com/the-internal-of-go-prompt-how-to-control-the-rich-terminal-ui-part-i-7d22bdfe6b9a)

## WASM

- [What Are Some Compelling Use Cases for WebAssembly?](https://medium.com/@OPTASY.com/what-are-some-compelling-use-cases-for-webassembly-top-6-ccc4b53474ee)

## Web development

- [Responsively](https://responsively.app/)
- [MDN](https://developer.mozilla.org/en-US/)

## JavaScript

- [awesome-javascript](https://github.com/sorrycc/awesome-javascript)
- [JSON Schema Reference](http://json-schema.org/understanding-json-schema/reference/index.html)
- [electronjs](https://www.electronjs.org)
- [Axios](https://axios-http.com)
- [Svelte](https://svelte.dev)
- [Wails](https://wails.io)
- [How to create an Electron application with Vite and Svelte](https://dev.to/olyno/how-to-create-an-electron-application-with-vite-im)
- [Svelte](https://svelte.dev)
- [TailwindCSS](https://tailwindcss.com/docs/guides/sveltekit)
- [Svelte, TailwindCSS, Electron and TypeScript](https://javascript.plainenglish.io/svelte-electron-typescript-daf35537e9f8)
- [Electron - Quick start](https://www.electronjs.org/docs/latest/tutorial/quick-start)
- [DaisyUI](https://daisyui.com)
- [How to Use Tailwind on a Svelte Site](https://css-tricks.com/how-to-use-tailwind-on-a-svelte-site/)
- [How to Set up Svelte using Vite, TypeScript and Tailwind CSS](https://www.section.io/engineering-education/svelte-with-vite-typescript-and-tailwind-css/)

## Carbon

- [Carbon Language's main repository](https://github.com/carbon-language/carbon-lang)

## Kotlin

- [Kotlin: A modern programming language that makes developers happier. ](https://kotlinlang.org/)
- [Compose Multiplatform](https://www.jetbrains.com/fr-fr/lp/compose-mpp/)

## CSS

- [Youtube -  Kevin Powell](https://www.youtube.com/kepowob)

## Rust

- [Rust repo](https://rustrepo.com/)
- [The Rust Programming Language](https://doc.rust-lang.org/stable/book/)
- [No Boilerplate - YouTube](https://www.youtube.com/c/NoBoilerplate)

## Servers

- [NGINX](https://www.nginx.com/)
- [gunicorn](https://gunicorn.org/)
- [I Am Gunicorn](https://medium.com/@mrmonkey82/gunicorn-5533293c913a)

## Behavior Driven Development

- [Cucumber](https://cucumber.io/)
- [Cucumber - 10 Minute Tutorial](https://cucumber.io/docs/guides/10-minute-tutorial/)
- [Appium](http://appium.io/)
- [Basic Appium and Cucumber - BDD Framework](https://medium.com/ralali-engineering/basic-appium-and-cucumber-bdd-framework-3eabef9ec033)
- [Appium Tutorial Step by Step Appium Automation](https://www.swtestacademy.com/appium-tutorial/)
- [Appium Mobile App Automation — Tutorial 1](https://medium.com/automationmaster/appium-mobile-app-automation-406bf8b0fd80)
- [Cucumber BDD (Part 1): Starting with Feature Mapping](https://medium.com/agile-vision/starting-with-bdd-for-collaborative-development-in-agile-environments-5fb034078b3c)
- [Cucumber BDD (Part 2): Creating a Sample Java Project with Cucumber, TestNG, and Maven](https://medium.com/agile-vision/cucumber-bdd-part-2-creating-a-sample-java-project-with-cucumber-testng-and-maven-127a1053c180)
- [Moving Across two devices using a single iOS/ Android Driver](https://discuss.appium.io/t/moving-across-two-devices-using-a-single-ios-android-driver/7082/7)
- [Appium Interview Questions](https://www.interviewbit.com/appium-interview-questions/)

## IDE / Code editors

- [Jetbrains](https://www.jetbrains.com)
	- [Key Promoter X](https://plugins.jetbrains.com/plugin/9792-key-promoter-x)

- [Sublime text](https://www.sublimetext.com/)
	- [Emoji](https://packagecontrol.io/packages/Emoji)
	- [MarkdownLivePreview](https://packagecontrol.io/packages/MarkdownLivePreview)

## Markdown

- [mermaid](https://mermaid-js.github.io/mermaid/#/)
- [PyMdown Extensions](https://facelessuser.github.io/pymdown-extensions/)
- [𝓜𝓪𝓻𝓴𝓸 - markdown parser](https://github.com/frostming/marko)

## Static site generator

- [Hugo - The world’s fastest framework for building websites](https://gohugo.io/)
- [A Fast Way to Create Docs using Hugo Framework](https://medium.com/@amirm.lavasani/a-fast-way-to-create-docs-using-hugo-framework-e49b7cb582af)

## Databases

- [Redis](https://redis.io/)

## Other

- [keep a changelog](https://keepachangelog.com/en/1.0.0/)
- [The Web Application Messaging Protocol](https://wamp-proto.org/)
- [OverAPI: Collecting All Cheat Sheets](https://overapi.com/)
- [Carbon](https://carbon.now.sh/)
- [Code Beautify](https://codebeautify.org/)
- [Roadmap](https://roadmap.sh/)
- [Resume](https://resume.io/)
- [Odoo: Open Source ERP](https://www.odoo.com/)
- [Nmap: The network mapper](https://nmap.org/)
- [The Test Pyramid In Practice](https://blog.octo.com/the-test-pyramid-in-practice-1-5/)
- [THE HTML PRESENTATION FRAMEWORK](https://revealjs.com/)
- [Smartbeat Software Testing, Monitoring, Delivery](https://smartbear.com/)
- [Zapier | Automation that moves you forward](https://zapier.com/)
- [Unlocking Slack’s Full Potential](https://medium.com/osedea/unlocking-slacks-full-potential-9a1841742bcc)
- [Code-based Diagramming](https://alpha2phi.medium.com/code-based-diagramming-6b1bcc732aab)
- [8 best opensource projects you should try out](https://medium.com/codex/8-best-opensource-projects-you-should-try-out-c0637d3c6ed3)
- [6 Best Courses to learn Nginx in depth](https://medium.com/javarevisited/best-courses-to-learn-nginx-in-36ed9ccca804)
- [YouTube - Fireship](https://www.youtube.com/c/Fireship)
- [Youtub - ByteByteGo](https://www.youtube.com/c/ByteByteGo)
- [The Web Application Messaging Protocol](https://wamp-proto.org/)
- [Freely available programming books ](https://github.com/EbookFoundation/free-programming-books)
- [Learn UML Class Diagram to Become a Better Software Developer ](https://www.learncsdesign.com/learn-uml-class-diagram-to-become-a-better-software-developer/)
- [45 Front End Developer Tools](https://levelup.gitconnected.com/45-front-end-developer-tools-e496b9c3503)
- [Top 87 Web Design Tools For Developer](https://niemvuilaptrinh.medium.com/top-87-web-design-tools-for-developer-816e52e3a521)
- [The next-gen web framework.](https://fresh.deno.dev/)
- [Chat App System Design](https://medium.com/@BalajiSA/whatsapp-system-design-3d8566bb2e6c)
- [Status Hero](https://statushero.com/)
- [How to study Cyber Security on your own for free?](https://medium.com/@kashishcharaya/how-to-study-cyber-security-on-your-own-for-free-a4f894dad919)
- [JSON Schema](https://json-schema.org/)
- [jsonschema](https://pypi.org/project/jsonschema/)
- [I Am Gunicorn](https://medium.com/@mrmonkey82/gunicorn-5533293c913a)
- [awesome-oss](https://github.com/sereneblue/awesome-oss)
- [30 seconds of code](https://www.30secondsofcode.org/)
- [Awesome-Selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)
- [Public APIs](https://github.com/public-apis/public-apis)
- [JSON Web Tokens](https://jwt.io/)
- [auth0](https://auth0.com/)
- [How to set up Linkedin Autoresponders, Automatic Replies, and Away Messages](https://aboundsocial.com/how-to-set-up-an-away-message-on-linkedin/)
- [Security for dummies: Protecting application secrets made easy](https://medium.com/prodopsio/security-for-dummies-protecting-application-secrets-made-easy-5ef3f8b748f7)
- [Semantic Versioning](https://semver.org)
- [Python Semantic Release](https://python-semantic-release.readthedocs.io/en/latest/)
- [Semantic release with Python, Poetry & GitHub Actions 🚀](https://mestrak.com/blog/semantic-release-with-python-poetry-github-actions-20nn)
- [Hugo Actually Explained (Websites, Themes, Layouts, and Intro to Scripting)](https://www.youtube.com/watch?v=ZFL09qhKi5I)
- [InterviewBit - Everything you need to crack your Next Tech Interview](https://www.interviewbit.com/)
- [Hackers and Slackers](https://hackersandslackers.com/)
- [Static Multicast Routing Daemon](https://troglobit.com/projects/smcroute/)
- [influxDB](https://www.influxdata.com/)
- [Test pyramid as a measurable code metric](https://levelup.gitconnected.com/test-pyramid-as-a-measurable-code-metric-b2363e95ebe8)
- [The Pyramid of Coding Principles](https://muhammad-rahmatullah.medium.com/the-pyramid-of-basic-coding-principles-e33b7c3cb316)
- [Wappalyzer - Technology profiler](https://chrome.google.com/webstore/detail/wappalyzer-technology-pro/gppongmhjkpfnbhagpmjfkannfbllamg?hl=fr)
- [Software architecture diagramming and patterns](https://www.educative.io/blog/software-architecture-diagramming-and-patterns)
- [flaviocopes](https://flaviocopes.com)
- [Blockchain Explained in 50 Lines of Code](https://medium.com/geekculture/blockchain-explained-in-50-lines-of-code-1dbf4eda0201)

## GUI design

- [Create atomic design systems with Pattern Lab](https://patternlab.io/)

## Web

### Web components

- [Lit](https://lit.dev/)
- [Component Driven User Interfaces](https://www.componentdriven.org/)
- [The Importance of Component Based UI Design](https://medium.com/swlh/the-importance-of-component-based-ui-design-666e5dfc7c1a)

## Android Development

- [Gradle vs Bazel for JVM Projects](https://blog.gradle.org/gradle-vs-bazel-jvm)
- [dumpsys](https://developer.android.com/studio/command-line/dumpsys)
- [Be Da Developer, Cucumber on Android](https://proandroiddev.com/be-da-developer-cucumber-on-android-cfd07773e59d)

## Tools

- [fzf is a general-purpose command-line fuzzy finder.](https://github.com/junegunn/fzf#using-homebrew)
- [Celery - Distributed Task Queue](https://docs.celeryq.dev/en/stable/)
- [HowTo: My Terminal & Shell setup - Hyper.js + ZSH + starship ☄️🔥](https://tjay.dev/howto-my-terminal-shell-setup-hyper-js-zsh-starship/)
- [Kite](https://www.kite.com)

### Terminal

- [Zsh](https://www.zsh.org)
- [Oh My ZSH!](https://ohmyz.sh)
- [Starship](https://starship.rs)
- [Warp](https://www.warp.dev)

## Get a job

- [talent.io](https://www.talent.io/p/en-fr/home)

## Fun

- [xkcd](https://xkcd.com/)
- [Markdown emoji](https://gist.github.com/rxaviers/7360908)
- [WTF Per Minute — An Actual Measurement for Code Quality](https://muhammad-rahmatullah.medium.com/wtf-per-minute-an-actual-measurement-for-code-quality-780914bf9d4b)
